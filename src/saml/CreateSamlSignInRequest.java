package saml;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class helps in generation of SAML SSO request as String.
 * 
 * @author shree-pt6203
 *
 */
public class CreateSamlSignInRequest {
	private static final Logger LOGGER = Logger.getLogger(CreateSamlSignInRequest.class.getName());

	/**
	 * Generate and returns the SSO XML request in string format.
	 * 
	 * @return sloRequestString The generated SSO request string.
   	 */
	public String createXmlReq() {
		String ssoRequestString = "";
		try {
			HashMap<String, String> attributes = new HashMap<String, String>();
			attributes.put("xmlns:saml2p", "urn:oasis:names:tc:SAML:2.0:protocol"); // No I18N
			attributes.put("AssertionConsumerServiceURL", Provider.providerList.get(0).aCS_URL); // No I18N
			attributes.put("Destination", Provider.providerList.get(0).iDP_SSO_URL); // No I18N
			attributes.put("ID", UUID.randomUUID().toString()); // No I18N
			attributes.put("IssueInstant", java.time.LocalDateTime.now().toString()); // No I18N
			attributes.put("ProtocolBinding", "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"); // No I18N
			attributes.put("Version", "2.0"); // No I18N

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();

			// Setting header AuthnRequest
			Element authnRequest = doc.createElement("saml2p:AuthnRequest"); // No I18N

			for (String key : attributes.keySet()) {
				authnRequest.setAttribute(key, attributes.get(key));
			}

			// Setting SAML issuer
			Element issuerElement = doc.createElement("saml2:Issuer"); // No I18N
			issuerElement.setAttribute("xmlns:saml2", "urn:oasis:names:tc:SAML:2.0:assertion");
			issuerElement.setTextContent(Provider.providerList.get(0).sPEntityID);

			authnRequest.appendChild(issuerElement);
			doc.appendChild(authnRequest);

			// Converting XML sign in request to string
			Transformer transformer = TransformerFactory.newInstance().newTransformer();
			DOMSource source = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			ssoRequestString = writer.toString();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Couldn't create SSO request xml", e);
		}
		if (ssoRequestString == "") {
			LOGGER.log(Level.WARNING, "Couldn't create SSO request xml");
		}
		return ssoRequestString;
	}
}