package saml;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * This class helps in generation of SAML SLO request as String.
 * 
 * @author shree-pt6203
 *
 */
public class CreateSamlLogoutRequest {
	private static final Logger LOGGER = Logger.getLogger(CreateSamlLogoutRequest.class.getName());

	/**
	 * Generate and returns the SLO XML request in string format.
	 * 
	 * @param nameIDValue This is the nameID of the user which is used in the generation of SLO request. This nameID is extracted from the AuthnResponse which is received for the SSO request.
	 * @return sloRequestString The generated SLO request string.
   	 */
	public String createXmlReq(String nameIDValue) {
		String sloRequestString = "";
		try {
			HashMap<String, String> attributes = new HashMap<String, String>();

			attributes.put("xmlns:samlp", "urn:oasis:names:tc:SAML:2.0:protocol"); // No I18N
			attributes.put("xmlns:saml", "urn:oasis:names:tc:SAML:2.0:assertion"); // No I18N
			attributes.put("ID", UUID.randomUUID().toString()); // No I18N
			attributes.put("Version", "2.0"); // No I18N
			attributes.put("IssueInstant", java.time.LocalDateTime.now().toString()); // No I18N
			attributes.put("Destination", Provider.providerList.get(0).iDP_SLO_URL); // No I18N
			attributes.put("NotOnOrAfter", java.time.LocalDateTime.now().plusMinutes(120).toString()); // No I18N
			attributes.put("Reason", "SP Logout"); // No I18N

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			Document doc = docBuilder.newDocument();

			// Setting header AuthnRequest
			Element logoutRequest = doc.createElement("samlp:LogoutRequest"); // No I18N

			for (String key : attributes.keySet()) {
				logoutRequest.setAttribute(key, attributes.get(key));
			}

			// Setting SAML issuer
			Element issuerElement = doc.createElement("saml:Issuer"); // No I18N
			issuerElement.setTextContent(Provider.providerList.get(0).sPEntityID);
			logoutRequest.appendChild(issuerElement);

			// Setting NameID
			Element nameID = doc.createElement("saml:NameID"); // No I18N
			nameID.setTextContent(nameIDValue);
			logoutRequest.appendChild(nameID);

			doc.appendChild(logoutRequest);

			// Converting XML sign in request to string
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			transformer.transform(source, result);
			sloRequestString = writer.toString();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Couldn't create SLO request xml", e);
		}
		if (sloRequestString == "") {
			LOGGER.log(Level.WARNING, "Couldn't create SLO request xml");
		}
		return sloRequestString;
	}
}