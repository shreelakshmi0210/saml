package saml;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.ArrayList;

/**
 * 
 * @author shree-pt6203
 *
 */
public abstract class Provider {
	protected String sPEntityID;
	protected String aCS_URL;
	protected String iDP_SSO_URL;
	protected String iDP_SLO_URL;
	protected String relayState;
	protected String signatureAlgorithm;
	protected PublicKey publicKey;
	protected PrivateKey privateKey;

	/**
	 * Contains the list of provider objects. <b>Note:</b> Default provider -
	 * provider object stored at index 0 in providerList in Provider class. Before
	 * configuration the providerList will be empty.
	 */
	public static List<Provider> providerList = new ArrayList<Provider>();

	/**
	 * The user must implement this abstract method for initializing the IdP's
	 * private key which is used for signing and decryption. <b>Note:</b> The
	 * private key must be stored in .key format.
	 */
	public abstract void setPublicKey();

	/**
	 * The user must implement this abstract method for initializing the IdP's
	 * public key which is used during signature verification. <b>Note:</b> The
	 * certificate must be stored in .cert format.
	 */
	public abstract void setPrivateKey();

	/**
	 * Sets the provider object as default provider by storing them at index 0 of
	 * providerList.
	 * 
	 * <b>Note:</b> The extended class must initialize the class variable of the
	 * Provider class.
	 * 
	 * @param provider This provided object is the object of the class which extends
	 *                 the Provider class and implements the abstract methods.
	 */
	public static void setProvider(Provider provider) {
		providerList.add(0, provider);
	}

	/**
	 * Removes the default provider at index 0 of the providerList if that exists.
	 */
	public static void removeProvider() {
		if (!providerList.isEmpty()) {
			providerList.remove(0);
		}
	}

	/**
	 * Clears the values stored in provideList variable.
	 */
	public static void emptyProviderList() {
		providerList.removeAll(providerList);
	}
}