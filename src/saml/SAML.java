package saml;

import java.io.ByteArrayInputStream;
import java.security.Key;
import java.security.Security;
import java.security.Signature;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.Init;
import org.apache.xml.security.encryption.EncryptedData;
import org.apache.xml.security.encryption.EncryptedKey;
import org.apache.xml.security.encryption.XMLCipher;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.onelogin.saml2.util.Util;

/**
 * Using this SAML class user can create Single Sign On (SSO) and Single Log Out
 * (SLO) requests for the IdP they have configured with.
 * 
 * @author shree-pt6203
 *
 */
public class SAML {
	static final Logger LOGGER = Logger.getLogger(SAML.class.getName());

	private Provider provider = Provider.providerList.get(0);
	String ssoRequestId;
	String nameIDValue;
	Map<String, String> assertionValues = new HashMap<String, String>();

	/**
	 * Returns a fully constructed SAML SSO request URL as a string.
	 * 
	 * @return ssoRequestURL
	 */
	public String generateSSORequestURL() {
		CreateSamlSignInRequest obj = new CreateSamlSignInRequest();
		String ssoRequest = obj.createXmlReq();
		ssoRequest = urlEncoder(deflateEncode(signRequest(ssoRequest)));
		String ssoRequestURL = provider.iDP_SSO_URL + "?SamlRequest=" + ssoRequest; // No I18N
		return ssoRequestURL;
	}

	/**
	 * Returns a fully constructed SAML SLO request URL as a string. The user can
	 * fetch the nameID received in the response using getNameID().
	 * 
	 * @see #getNameID()
	 * 
	 * @param nameID The unique nameID of the user
	 * @return sloRequestURL
	 */
	public String generateSLORequestURL(String nameID) {
		CreateSamlLogoutRequest obj = new CreateSamlLogoutRequest();
		String sloRequest = obj.createXmlReq(nameID);
		sloRequest = urlEncoder(deflateEncode(sloRequest));
		String stringToSign = "SAMLRequest=" + sloRequest + "&RelayState=" + provider.relayState + "&SigAlg=" + provider.signatureAlgorithm; // No I18N
		String signature = urlEncoder(signRequest(stringToSign));
		String sloRequestURL = provider.iDP_SLO_URL + "?SAMLRequest=" + sloRequest + "&SigAlg=" // No I18N
				+ provider.signatureAlgorithm + "&Signature=" + signature + "&RelayState=" + provider.relayState; // No I18N
		return sloRequestURL;
	}

	/**
	 * Converts and returns the passed string value as Document object.
	 * 
	 * @param data The string to be converted.
	 * @return Document
	 */
	public Document convertStringToXMLDoc(String data) {
		Document doc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			doc = builder.parse(new ByteArrayInputStream(data.getBytes()));
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Conversion of string to document failed");
		} finally {
			if (doc == null) {
				LOGGER.log(Level.SEVERE, "Conversion of string to document failed");
			}
		}
		return doc;
	}

	/**
	 * Returns the signed xmlString.
	 * 
	 * <p>
	 * <b>Note:</b> Signature object is used to sign the data using the private key
	 * provider by the user.
	 * 
	 * @param xmlString The string to be signed
	 * @return The signed string
	 */
	public String signRequest(String xmlString) {
		String signedValue = null;
		try {
			Signature signature = Signature.getInstance("SHA1withRSA");
			signature.initSign(provider.privateKey);
			signature.update(xmlString.getBytes("UTF-8")); // No I18N
			byte[] signatureBytes = signature.sign();
			signedValue = Base64.getEncoder().encodeToString(signatureBytes);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Signing process failed", e);
		}
		return signedValue;
	}

	/**
	 * Returns the passed string value after base64 encoding and deflating.
	 * 
	 * @param data The string to be deflated and base64 encoded.
	 * @return The deflated and base64 encoded data
	 */
	public String deflateEncode(String data) {
		String result = null;
		try {
			result = Util.deflatedBase64encoded(data);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Issue in deflate and encoding SAML SSO request", e);
		}
		return result;
	}

	/**
	 * Returns the passed string value after base64 inflating and base64 decoding.
	 * 
	 * @param data The string to be inflated and base64 decoded.
	 * @return The inflated and base64 decoded data
	 */
	public String inflateDecode(String data) {
		String result = Util.base64decodedInflated(data);
		return result;
	}

	/**
	 * Returns the URL encoded string.
	 * 
	 * @param data The string to be URL encoded.
	 * @return The URL encoded string
	 */
	public String urlEncoder(String data) {
		String result = Util.urlEncoder(data);
		return result;
	}

	/**
	 * Converts the request string into Document object. Validates the success
	 * status. Decrypt the response is encrypted. Fetch the assertion values using
	 * fetchAssertionValues(). Verifies signature (either response, assertion or
	 * both) if signed. Returns the validation(s) result as boolean.
	 * 
	 * @param samlResponse The XML Response string
	 * @return true on success; otherwise false
	 */
	public boolean isSSOResponseSuccessful(String samlResponse) {
		if (samlResponse == null || samlResponse == "") {
			LOGGER.log(Level.WARNING, "Empty or Null response");
			return false;
		}
		boolean responseStatus = false;
		boolean assertionStatus = false;
		boolean successStatus = false;
		boolean decryptionStatus = false;

		Document doc = convertStringToXMLDoc(samlResponse);

		Node statusCode = doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:protocol", "StatusCode").item(0).getAttributes() // No I18N
				.getNamedItem("Value"); // No I18N
		if (statusCode.getNodeValue().contains("urn:oasis:names:tc:SAML:2.0:status:Success")) {
			successStatus = true;

			// Verify response signature if signed
			responseStatus = isResponseSignatureValid(doc);

			// Check whether assertion is encrypted and decryt if encryted
			doc = isAssertionEncrypted(doc);

			if (doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Assertion").getLength() == 1) {
				fetchAssertionValues(doc);
				decryptionStatus = true; // signifies successful decryption if encrypted.
			} else {
				LOGGER.log(Level.WARNING, "Assertion is not decrypted or not found");
			}

			// Verify assertion signature if signed
			assertionStatus = isAssertionSignatureValid(doc);
		}
		return successStatus && assertionStatus && responseStatus && decryptionStatus;
	}

	/**
	 * Validates the success status. Verifies response signature if signed. Returns
	 * the validation result as boolean value.
	 * 
	 * @param samlResponse The XML response string
	 * @return true on success; otherwise false
	 */
	public boolean isSLOResponseSuccessful(String samlResponse) {
		if (samlResponse == null || samlResponse == "") {
			LOGGER.log(Level.WARNING, "Empty or Null response");
			return false;
		}
		if (samlResponse.contains("urn:oasis:names:tc:SAML:2.0:status:Success")
				&& isResponseSignatureValid(samlResponse)) {
			return true;
		}
		return false;
	}

	/**
	 * Extract the NameID value from the Subject element. Extract the Attribute
	 * values from the AttributeStatement element. User can get the assertion using
	 * getAssertionValues().
	 * 
	 * @see #getAssertionValues()
	 * 
	 * @param doc The XML document with SAML response
	 */
	public void fetchAssertionValues(Document doc) {
		// Extract the NameID value from the Subject element
		NodeList subjectList = doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Subject"); // No I18N
		Element subjectElement = (Element) subjectList.item(0);
		String nameIdValue = subjectElement.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "NameID").item(0) // No I18N
				.getTextContent();
		setNameID(nameIdValue);

		// Extract the Attribute values from the AttributeStatement element
		NodeList attributeList = doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Attribute"); // No I18N
		for (int i = 0; i < attributeList.getLength(); i++) {
			Element attributeElement = (Element) attributeList.item(i);
			String attributeName = attributeElement.getAttribute("Name");
			String attributeValue = attributeElement.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "AttributeValue") // No I18N
					.item(0).getTextContent();
			assertionValues.put(attributeName, attributeValue);
		}
	}

	private void setNameID(String value) {
		nameIDValue = value;
	}

	/**
	 * Returns the nameID which is fetched from the SSO response received.
	 * 
	 * @return nameIDValue
	 */
	public String getNameID() {
		return nameIDValue;
	}

	/**
	 * Returns the assertion fetched from the response after the user invokes
	 * fetchAssertionValues().
	 * 
	 * @see #fetchAssertionValues(Document)
	 * 
	 * @return assertionValues The assertion's attribute name and its value are
	 *         stored in a Map object.
	 */
	public Map<String, String> getAssertionValues() {
		return assertionValues;
	}

	private Document isAssertionEncrypted(Document doc) {
		if (doc != null) {
			if (doc.getElementsByTagName("xenc:EncryptedData").getLength() != 0) {
				doc = decryptResponse(doc);
			} else {
				LOGGER.log(Level.INFO, "Response Asertion is UNENCRYPTED");
			}
		} else {
			LOGGER.log(Level.SEVERE, "Response document is empty... Can't proceed further");
		}
		return doc;
	}

	/**
	 * Decrypts the encrypted response in the given XML document.
	 * 
	 * @param doc The XML document containing the encrypted response
	 * @return The decrypted XML Document with the response
	 */
	public Document decryptResponse(Document doc) {
		try {
			Security.addProvider(new BouncyCastleProvider());

			Element encryptedDataElement = (Element) doc
					.getElementsByTagNameNS("http://www.w3.org/2001/04/xmlenc#", "EncryptedData").item(0); // No I18N
			Key secretKey = loadDecryptionKey(doc);

			Element encryptedKeyElement = (Element) doc
					.getElementsByTagNameNS("http://www.w3.org/2001/04/xmlenc#", "EncryptedKey").item(0); // No I18N

			XMLCipher xmlCipher = XMLCipher.getInstance(XMLCipher.AES_256);
			xmlCipher.init(XMLCipher.DECRYPT_MODE, secretKey);
			xmlCipher.loadEncryptedData(doc, encryptedDataElement);
			xmlCipher.loadEncryptedKey(doc, encryptedKeyElement);

			doc = xmlCipher.doFinal(doc, encryptedDataElement);
			Element parentElement = (Element) doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:protocol", "Response").item(0); // No I18N
			Element assertionElement = (Element) doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Assertion") // No I18N
					.item(0);
			Element encryptedAssertionElement = (Element) doc
					.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "EncryptedAssertion").item(0); // No I18N
			parentElement.replaceChild(assertionElement, encryptedAssertionElement);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Decryption failed", e);
		}
		return doc;
	}

	private Key loadDecryptionKey(Document doc) {
		Key key = null;

		try {
			Element e = (Element) doc.getElementsByTagNameNS("http://www.w3.org/2001/04/xmlenc#", "EncryptedKey") // No I18N
					.item(0);

			Init.init();
			XMLCipher cipher = XMLCipher.getInstance();
			cipher.init(XMLCipher.DECRYPT_MODE, null);
			EncryptedData encryptedData = cipher.loadEncryptedData(doc, e);
			if (encryptedData == null) {
				throw new Exception("EncryptedData is null");
			} else if (encryptedData.getKeyInfo() == null) {
				throw new Exception("KeyInfo of the EncryptedData is null");
			}

			// get encrypted key data from document
			EncryptedKey ek = encryptedData.getKeyInfo().itemEncryptedKey(0);
			if (ek == null) {
				e = (Element) doc.getElementsByTagNameNS("http://www.w3.org/2001/04/xmlenc#", "EncryptedKey").item(0); // No I18N
				ek = cipher.loadEncryptedKey(doc, e);
			}

			if (ek != null) {
				// Initialize cipher for unwrap
				XMLCipher keyCipher = XMLCipher.getInstance();
				keyCipher.init(XMLCipher.UNWRAP_MODE, provider.privateKey);
				key = keyCipher.decryptKey(ek, encryptedData.getEncryptionMethod().getAlgorithm());
			}
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Failed to decrypt symmetric key", e);
		}
		return key;
	}

	/**
	 * Returns the status of assertion signature verification if signed.
	 * 
	 * @param doc The XML document with/without signed assertion
	 * @return true on success; otherwise false
	 */
	public boolean isAssertionSignatureValid(Document doc) {
		boolean isSignatureValid = false;
		NodeList nodeList = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature"); // No I18N
		Node node = null;

		for (int index = 0; index < nodeList.getLength(); index++) {
			Node tempNode = nodeList.item(index);
			if (tempNode.getParentNode().getLocalName().equals("Assertion")) {
				node = tempNode;
				break;
			}
		}

		if (node == null) {
			LOGGER.log(Level.INFO, "The assertion is not signed");
			isSignatureValid = true;
			return isSignatureValid;
		}

		isSignatureValid = signatureValidation(node);
		return isSignatureValid;
	}

	/**
	 * Returns the status of assertion signature verification if signed.
	 * 
	 * <p>
	 * <b>Note:</b> Converts the response string into document object and invokes
	 * isAssertionSignatureValid(Document).
	 * 
	 * @see #isAssertionSignatureValid(Document)
	 * 
	 * @param response The XML String with/without signed assertion.
	 * @return true on success; otherwise false
	 */
	public boolean isAssertionSignatureValid(String response) {
		Document doc = convertStringToXMLDoc(response);
		return isAssertionSignatureValid(doc);
	}

	/**
	 * Returns the status of response signature verification if signed.
	 * 
	 * @param doc The XML document with/without signed response
	 * @return true on success; otherwise false
	 */
	public boolean isResponseSignatureValid(Document doc) {
		boolean isSignatureValid = false;
		NodeList nodeList = doc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature"); // No I18N
		Node node = null;

		for (int index = 0; index < nodeList.getLength(); index++) {
			node = nodeList.item(index);
			if (node.getParentNode().getLocalName().equals("Response")
					|| node.getParentNode().getLocalName().equals("LogoutResponse")) {
				break;
			} else {
				node = null;
			}
		}

		if (node == null) {
			LOGGER.log(Level.INFO, "The response is not signed");
			isSignatureValid = true;
			return isSignatureValid;
		}

		isSignatureValid = signatureValidation(node);
		return isSignatureValid;
	}

	/**
	 * Returns the status of response signature verification if signed.
	 * 
	 * <p>
	 * <b>Note:</b> Converts the response string into document object and invokes
	 * isResponseSignatureValid(Document).
	 * 
	 * @see #isResponseSignatureValid(Document)
	 * 
	 * @param response The XML String with/without signed response.
	 * @return true on success; otherwise false
	 */
	public boolean isResponseSignatureValid(String response) {
		Document doc = convertStringToXMLDoc(response);
		return isResponseSignatureValid(doc);
	}

	private boolean signatureValidation(Node node) {
		boolean isSignatureValid = false;
		try {
			// Create a DOMValidateContext with the public key from the certificate
			XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM");
			DOMValidateContext valContext = new DOMValidateContext(provider.publicKey, node);

			// Create the XMLSignature object
			XMLSignature signature = sigFactory.unmarshalXMLSignature(valContext);

			Element el = (Element) node.getParentNode();
			el.setIdAttribute("ID", true); // No I18N
			valContext.setProperty("javax.xml.crypto.dsig.cacheReference", Boolean.TRUE);

			// Verify the signature
			isSignatureValid = signature.validate(valContext);
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Couldn't verify signature", e);
		}
		return isSignatureValid;
	}
}