package test;

import org.testng.TestNG;

import saml.Provider;

public class TestRunner {

	public static void main(String[] args) {
		Provider.setProvider(new MockProvider());
		TestNG testNG = new TestNG();
		testNG.setTestClasses(new Class[] {ProviderTest.class, CreateSamlSignInRequestTest.class, CreateSamlLogoutRequestTest.class, SAMLTest.class});
        testNG.run();	
	}
}