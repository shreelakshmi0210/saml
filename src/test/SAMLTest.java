package test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.w3c.dom.Document;

import com.onelogin.saml2.util.Util;

import saml.Provider;
import saml.SAML;

public class SAMLTest {
	private SAML saml;

	@BeforeClass
	public void setup() {
		Provider.setProvider(new MockProvider());
		saml = new SAML();
	}
	
	@AfterClass
	public void tearUp() {
		Provider.removeProvider();
	}
  
	@Test
	public void signRequestTest() {
		String data = " Test data"; // No I18N
		String expectedvalue = "IfOW64+tyLoSq5K2Rq+7rmzJwA7WQOpeUoRQWsfDTpaiuvV/8g1NL5n0LUkdMjnCgNJ8F1KbCEh4bc4CuseZ/EMtk2GleMaMzlXEFS/VsU7UGmrMafsOrPh3WRmqosTF7Q8vO9WyBtY+6jV6BlD7jUA2Fa1IoUYS6jPKmOEVkc/QtvkveEJW0+cz1X2hXLbB2owVk32U4w+2pX47LQSNvxGIf/Zm841cCBIvgfh0BpRcBdHcKVrL/QndxR0S9K3QwvKtdAlI+imwRYAd1S5A3yjjXWQmKJTILxMB/m77hDMDv68BtYb/JMH+YTm0KUkEqHyesAxLEXWRBd8qx3I1YA=="; // No I18N
		String actualValue = saml.signRequest(data);
		assertEquals(actualValue, expectedvalue);
	}
	
	@Test
	public void deflateEncodeTest() throws IOException {
		String data = " Test data"; // No I18N
		String expectedValue = Util.deflatedBase64encoded(data);
		String actualValue = saml.deflateEncode(data);
		assertNotNull(actualValue);
		assertEquals(actualValue, expectedValue);
	}

	@Test
	public void inflateDecodeTest() {
		String data = " Test data"; // No I18N
		String expectedValue = Util.base64decodedInflated(data);
		String actualValue = saml.inflateDecode(data);
		assertNotNull(actualValue);
		assertEquals(actualValue, expectedValue);
	}

	@Test
	public void urlEncoderTest() {
		String data = " Test data"; // No I18N
		String expectedValue = Util.urlEncoder(data);
		String actualValue = saml.urlEncoder(data);
		assertNotNull(actualValue);
		assertEquals(actualValue, expectedValue);
	}
	
	@Test
	public void generateSSORequestURLTest() {
		String sSORequestUrl = saml.generateSSORequestURL();
		assertNotNull(sSORequestUrl);
		assertTrue(sSORequestUrl.contains("?SamlRequest=")); // No I18N
	}

	/*
	 * An unsigned SAML Response with an unsigned Assertion 
	 * An unsigned SAML Response with a signed Assertion 
	 * A signed SAML Response with an unsigned Assertion 
	 * A signed SAML Response with a signed Assertion 
	 * An unsigned SAML Response with an encrypted Assertion 
	 * An unsigned SAML Response with an encrypted signed Assertion 
	 * A signed SAML Response with an encrypted Assertion
	 * A signed SAML Response with an encrypted signed Assertion
	 */

	@DataProvider(name = "SSOResponsesValid") // No I18N
	public String[] sSOResponsesValid() {
		String[] responses = Responses.ssoResponsesValid;
		return responses;
	}

	@Test(dataProvider = "SSOResponsesValid") // No I18N
	public void isSSOResponseSuccessfulTestWhenValid(String data) {
		boolean responseValidation = saml.isSSOResponseSuccessful(data);
		assertTrue(responseValidation);

		Map<String, String> expectedAssertionValues = Responses.assertionValues;
		Map<String, String> actualAssertionValues = saml.getAssertionValues();
		assertEquals(actualAssertionValues, expectedAssertionValues);

		String nameID = saml.getNameID();
		assertEquals(nameID, "shree0210"); // No I18N
	}

	@DataProvider(name = "SSOResponsesInvalid") // No I18N
	public String[] sSOResponsesInvalid() {
		String[] responses = Responses.ssoResponsesInvalid;
		return responses;
	}

	@Test(dataProvider = "SSOResponsesInvalid") // No I18N
	public void isSSOResponseSuccessfulTestWhenInvalid(String data) {
		boolean responseValidation = saml.isSSOResponseSuccessful(data);
		assertFalse(responseValidation);
	}

	@Test
	public void generateSLORequestURLTest() {
		String sLORequestUrl = saml.generateSLORequestURL("Test Name ID"); // No I18N
		assertNotNull(sLORequestUrl);
		assertTrue(sLORequestUrl.contains("?SAMLRequest=")); // No I18N
		assertTrue(sLORequestUrl.contains("&SigAlg=")); // No I18N
		assertTrue(sLORequestUrl.contains("&Signature=")); // No I18N
		assertTrue(sLORequestUrl.contains("&RelayState=")); // No I18N
	}

	/*
	 * A Logout Response without the signature
	 * A Logout Response with the signature embedded (HTTP-POST binding)
	 */

	@DataProvider(name = "SLOResponsesValid") // No I18N
	public String[] sLOResponsesValid() {
		String[] responses = Responses.sloResponsesValid;
		return responses;
	}

	@Test(dataProvider = "SLOResponsesValid") // No I18N
	public void isSLOResponseSuccessfulTestWhenValid(String data) {
		boolean responseValidation = saml.isSLOResponseSuccessful(data);
		assertTrue(responseValidation);
	}

	@DataProvider(name = "SLOResponsesInvalid") // No I18N
	public String[] sLOResponsesInvalid() {
		String[] responses = Responses.sloResponsesInvalid;
		return responses;
	}

	@Test(dataProvider = "SLOResponsesInvalid") // No I18N
	public void isSLOResponseSuccessfulTestWhenInvalid(String data) {
		boolean responseValidation = saml.isSLOResponseSuccessful(data);
		assertFalse(responseValidation);
	}

	@Test
	public void isAssertionSignatureValidTest() {
		boolean assertionValidationValid = saml.isAssertionSignatureValid(Responses.assertionValidationValid);
		assertTrue(assertionValidationValid);
		
		assertionValidationValid = saml.isAssertionSignatureValid(Responses.unsignedResponseUnsignedAssertion);
		assertTrue(assertionValidationValid);
		
		boolean assertionValidationInvalid = saml.isAssertionSignatureValid(Responses.assertionValidationInvalid);
		assertFalse(assertionValidationInvalid);
		
		assertionValidationInvalid = saml.isAssertionSignatureValid(Responses.invalidSSOResponse3);
		assertFalse(assertionValidationInvalid);
	}

	@Test
	public void isResponseSignatureValidTest() {
		boolean responseValidationValid = saml.isResponseSignatureValid(Responses.responseValidationValid);
		assertTrue(responseValidationValid);
		
		responseValidationValid = saml.isResponseSignatureValid(Responses.signedResponseUnsignedAssertion);
		assertTrue(responseValidationValid);
		
		boolean responseValidationInvalid = saml.isResponseSignatureValid(Responses.responseValidationInvalid);
		assertFalse(responseValidationInvalid);
		
		responseValidationInvalid = saml.isResponseSignatureValid(Responses.invalidSSOResponse2);
		assertFalse(responseValidationInvalid);
	}
	
	@Test
	public void fetchAssertionValuesTest() {
		saml.fetchAssertionValues(Responses.assertionValidationValid);
		Map<String, String> expectedAssertionValues = Responses.assertionValues;
		Map<String, String> actualAssertionValues = saml.getAssertionValues();
		assertEquals(actualAssertionValues, expectedAssertionValues);

		String nameID = saml.getNameID();
		assertEquals(nameID, "shree0210"); // No I18N
	}
	
	@Test
	public void decryptResponseTest() {
		boolean decryptionStatus = false;
		
		Document doc = saml.decryptResponse(Responses.invalidEncryptedDoc);
		if (doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Assertion").getLength() == 1) { // No I18N
			decryptionStatus = true;
		}
		assertFalse(decryptionStatus);
		
		doc = saml.decryptResponse(Responses.validEncryptedDoc);
		if (doc.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Assertion").getLength() == 1) { // No I18N
			decryptionStatus = true;
		}
		assertTrue(decryptionStatus);
	}
	
	@Test
	public void convertStringToXMLDocTest() {
		Document doc = saml.convertStringToXMLDoc(Responses.unsignedResponseUnsignedAssertion);
		assertTrue(doc.hasChildNodes());
	}
}