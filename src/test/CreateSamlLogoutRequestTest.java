package test;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import saml.CreateSamlLogoutRequest;
import saml.Provider;

public class CreateSamlLogoutRequestTest {
	@Test
	public void createXmlReqTest() throws Exception {
		Provider.setProvider(new MockProvider());
		CreateSamlLogoutRequest createSamlLogoutRequest = new CreateSamlLogoutRequest();
		String sLORequestString = createSamlLogoutRequest.createXmlReq("Mock Name ID"); // No I18N
		String expectedXmlReq = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><samlp:LogoutRequest xmlns:samlp=\"urn:oasis:names:tc:SAML:2.0:protocol\" xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\" Destination=\"Mock IdP SLO URL\" ID=\"\" IssueInstant=\"\" NotOnOrAfter=\"\" Reason=\"SP Logout\" Version=\"2.0\"><saml:Issuer>Mock SP Entity ID</saml:Issuer><saml:NameID>Mock Name ID</saml:NameID></samlp:LogoutRequest>"; // No I18N

		String uuidPattern = "\\b[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}\\b"; // No I18N
		String dateTimePattern = "\\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3}\\b"; // No I18N

		boolean areEqual = sLORequestString.replaceAll(uuidPattern, "").replaceAll(dateTimePattern, "").trim()
				.equals(expectedXmlReq.trim());

		assertTrue(areEqual);
		Provider.removeProvider();
	}
}
