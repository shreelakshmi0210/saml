package test;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.util.List;
import org.testng.annotations.Test;

import saml.Provider;

public class ProviderTest {
	@Test
	public void setProviderTest() {
		Provider.setProvider(new MockProvider());
		List<Provider> providerList = Provider.providerList;
		assertNotNull(providerList.get(0));
		assertTrue(providerList.size() > 0);
		Provider.removeProvider();
	}

	@Test
	public void removeProviderTest() {
		boolean status = false;
		Provider tempProvider = null;
		if (Provider.providerList.size() != 0) {
			tempProvider = Provider.providerList.get(0);
			status = true;
		}
		List<Provider> providerList = Provider.providerList;
		int initialSize = providerList.size();
		Provider.removeProvider();
		int presentSize = providerList.size();
		assertTrue(presentSize <= initialSize);
		if (status) {
			Provider.setProvider(tempProvider);
		}
	}

	@Test
	public void emptyProviderListTest() {
		Provider tempProvider = Provider.providerList.get(0);
		Provider.emptyProviderList();
		assertTrue(Provider.providerList.size() == 0);
		Provider.setProvider(tempProvider);
	}
}