package test;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import saml.Provider;

public class MockProvider extends Provider{
	private static final Logger LOGGER = Logger.getLogger(Provider.class.getName());

	public MockProvider() {
		sPEntityID = "Mock SP Entity ID"; // No I18N
		aCS_URL = "Mock ACS URL"; // No I18N
		iDP_SSO_URL = "Mock IdP SSO URL"; // No I18N
		iDP_SLO_URL = "Mock IdP SLO URL"; // No I18N
		relayState = "Mock Relay State"; // No I18N
		signatureAlgorithm = "Mock Signature Algorithm"; // No I18N
		setPublicKey();
		setPrivateKey();
	}

	@Override
	public void setPublicKey() {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(
					"/home/local/ZOHOCORP/shree-pt6203/workspace/SAML/resources/credentials/okta.cert"); // No I18N
			CertificateFactory cf = CertificateFactory.getInstance("X509");
			Certificate cert = cf.generateCertificate(fis);
			publicKey = cert.getPublicKey();
			fis.close();
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Error in retrieving public key", e);
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				LOGGER.log(Level.FINE, "Couldn't close FileInputStream(fis)");
			}
		}
	}

	@Override
	public void setPrivateKey() {
		try {
			Path publicKeyPath = Paths.get(
					"/home/local/ZOHOCORP/shree-pt6203/workspace/SAML/resources/credentials/private_key.key"); // No I18N
			String privateKeyString = "";
			for (String line : Files.readAllLines(publicKeyPath)) {
				privateKeyString += line;
			}
			privateKeyString = privateKeyString.replaceAll("-----BEGIN PRIVATE KEY-----", ""); // No I18N
			privateKeyString = privateKeyString.replace("-----END PRIVATE KEY-----", ""); // No I18N
			byte[] privateKeyBytes = Base64.getDecoder().decode(privateKeyString);
			privateKey = KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(privateKeyBytes));
		} catch (Exception e) {
			LOGGER.log(Level.WARNING, "Error in retrieving private key", e);
		}
	}
}
