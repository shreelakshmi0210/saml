package test;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.Test;

import saml.CreateSamlSignInRequest;
import saml.Provider;

public class CreateSamlSignInRequestTest {
	@Test
	public void createXmlReqTest() throws Exception {
		Provider.setProvider(new MockProvider());
		CreateSamlSignInRequest createSamlSignInRequest = new CreateSamlSignInRequest();
		String sSORequestString = createSamlSignInRequest.createXmlReq();
		String expectedXmlReq = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><saml2p:AuthnRequest xmlns:saml2p=\"urn:oasis:names:tc:SAML:2.0:protocol\" AssertionConsumerServiceURL=\"Mock ACS URL\" Destination=\"Mock IdP SSO URL\" ID=\"\" IssueInstant=\"\" ProtocolBinding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" Version=\"2.0\"><saml2:Issuer xmlns:saml2=\"urn:oasis:names:tc:SAML:2.0:assertion\">Mock SP Entity ID</saml2:Issuer></saml2p:AuthnRequest>"; // No I18N

		String uuidPattern = "\\b[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}\\b"; // No I18N
		String dateTimePattern = "\\b[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3}\\b"; // No I18N

		boolean areEqual = sSORequestString.replaceAll(uuidPattern, "").replaceAll(dateTimePattern, "").trim()
				.equals(expectedXmlReq.trim());

		assertTrue(areEqual);
		Provider.removeProvider();
	}
}
